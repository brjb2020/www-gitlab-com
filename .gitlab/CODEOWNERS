# https://docs.gitlab.com/ee/user/project/code_owners.html

# Each CODEOWNERS entry requires at least 2 or more individuals OR at least 1 group with access to the www-gitlab-com repository.

# The order in which the paths are defined is significant: the last pattern that matches a given path will be used to find the code owners.

[CODEOWNERS]
.gitlab/CODEOWNERS @timzallmann @david @gitlab-com/ceo-chief-of-staff-team

# NB Do not move this entry from the top of this file

^[Default Description Templates]
.gitlab/issue_templates/Default.md @sytses @gitlab-com/ceo-chief-of-staff-team
.gitlab/merge_request_templates/Default.md @sytses @gitlab-com/ceo-chief-of-staff-team

# The handbook has now been migrated
^[Handbook]
/sites/handbook/source/handbook/ @jamiemaynard

^[Active Tests]
/sites/uncategorized/source/pricing/ @mpreuss22

^[Team Page & Pets]
/data/team_members/ @mpatel8 @ameeks @ashjammers @Mowry @alex_venter @shensiek @mianaranjo-lepe @reneexiong @cgudgenov
/sites/uncategorized/source/images/team/ @mpatel8 @ameeks @ashjammers @Mowry @alex_venter @shensiek @mianaranjo-lepe @reneexiong @cgudgenov
/sites/uncategorized/source/company/team-pets/ @mpatel8 @ameeks @ashjammers @Mowry @alex_venter @shensiek @mianaranjo-lepe @reneexiong @cgudgenov

[Tech Stack]
/data/tech_stack.yml @rrea1 @marc_disabatino

^[Compensation]
/data/currency_conversions.yml @wendybarnes @mwilkins
/data/entity_mapper.yml @wendybarnes @anechan
/data/country_payment_information.yml @wendybarnes @brobins
/data/variable_pay_frequency.yml @mwilkins

^[Conversion]
/sites/uncategorized/source/stages-devops-lifecycle/ @mpreuss22

^[Engineering]
/data/projects.yml @joergheilig
/data/schemas/team_member.schema.json @joergheilig @meks @rymai

^[Learn at gitlab]
/source/images/learn/ @cmestel

^[Marketing Community]
/data/heroes.yml @esalvadorp @johncoghlan
/data/heroes_contributions.yml @esalvadorp @johncoghlan
/data/speakers.yml @johncoghlan @abuango
/data/speakers_requirements.yml @johncoghlan @abuango

^[Product]
/data/categories.yml @david
/data/sections.yml @david @joergheilig
/data/stages.yml @david @joergheilig
/source/direction/ @david

^[Product - Dev Section]
/source/direction/dev/ @david @gl-product-leadership
/source/direction/dev/strategies/ @david @gl-product-leadership
/source/direction/manage/ @uchetta @david @gl-product-leadership
/source/direction/manage/foundations/ @david @uchetta @gl-product-leadership
/source/direction/manage/import_and_integrate/ @david @uchetta @m_frankiewicz @gl-product-leadership
/source/direction/plan/devops-reports/ @hsnir1 @david @gl-product-leadership
/source/direction/manage/code-analytics/ @hsnir1 @david @gl-product-leadership
/source/direction/manage/foundations/navigation_settings/ @gl-product-leadership @uchetta @david
/source/direction/manage/foundations/design_system/ @gl-product-leadership @uchetta @david
/source/direction/manage/foundations/gitlab_docs/ @gl-product-leadership @uchetta @david
/source/direction/plan/ @mushakov @david @gl-product-leadership
/source/direction/plan/value_stream_management/ @hsnir1 @david @gl-product-leadership
/source/direction/plan/portfolio_management/ @mushakov @david @amandarueda @gl-product-leadership
/source/direction/plan/design_management/ @mushakov @david @amandarueda @gl-product-leadership
/source/direction/plan/project_management/ @mushakov @david @gweaver @gl-product-leadership
/source/direction/plan/project_management/team_planning/ @mushakov @david @gweaver @gl-product-leadership
/source/direction/plan/knowledge/content_editor/ @mushakov @david @mmacfarlane @gl-product-leadership
/source/direction/plan/knowledge/ @mushakov @david @mmacfarlane @gl-product-leadership
/source/direction/plan/knowledge/wiki/ @mushakov @david @mmacfarlane @gl-product-leadership
/source/direction/plan/knowledge/pages/ @mushakov @david @mmacfarlane @gl-product-leadership
/source/direction/create/ @derekferguson @david @gl-product-leadership
/source/direction/create/source_code_management/ @david @derekferguson @mcbabin @gl-product-leadership
/source/direction/create/code_review_workflow/ @derekferguson @david @phikai @gl-product-leadership
/source/direction/create/ide/web_ide/ @derekferguson @david @ericschurter @gl-product-leadership
/source/direction/create/code_creation/code_suggestions/ @derekferguson @kbychu @hbenson @gl-product-leadership
/source/direction/create/editor_extensions/ @derekferguson @dashaadu @david @hbenson @phikai @gl-product-leadership
/source/direction/create/gitlab_cli/ @derekferguson @david @phikai @gl-product-leadership
/source/direction/create/source_code_management/source_editor/ @derekferguson @mcbabin @david @gl-product-leadership
/source/direction/create/gitter/ @derekferguson @ericschurter @david @gl-product-leadership

^[Product Section - Ops]
/source/direction/ops/ @david @mflouton
/source/direction/delivery/ @nagyv-gitlab
/source/direction/package/ @trizzi
/source/direction/verify/ @jreporter
/source/direction/verify/code_testing/ @jocelynjane @jreporter
/source/direction/verify/build_artifacts/ @jocelynjane @jreporter
/source/direction/verify/performance_testing/ @jocelynjane @jreporter
/source/direction/verify/review_apps/ @jocelynjane @jreporter
/source/direction/verify/runner_saas/ @jreporter @gabrielengel_gl

^[Product Section - Sec]
/source/direction/security/ @hbenson @gl-product-leadership
/source/direction/secure/ @sarahwaldner @hbenson
/source/direction/secure/static-analysis/ @connorgilbert @sarahwaldner
/source/direction/secure/dynamic-analysis/ @smeadzinger @sarahwaldner
/source/direction/secure/vulnerability-research/ @sarahwaldner
/source/direction/secure/composition-analysis/ @johncrowley @sarahwaldner
/source/direction/govern/ @sam.white @hbenson @g.hickman @abellucci @jstava @hsutor
/source/direction/govern/threat_insights/  @abellucci @sam.white @hbenson
/source/direction/govern/security_policies/ @g.hickman @sam.white @hbenson
/source/direction/govern/compliance/ @g.hickman @sam.white @hbenson
/source/direction/govern/anti-abuse/ @jstava @sam.white @hbenson
/source/direction/supply-chain/ @sam.white @hbenson

^[Product - Analytics Section]
/source/direction/monitor/ @david @justinfarris @stkerr
/source/direction/monitor/analytics-instrumentation/ @david @justinfarris @stkerr @tjayaramaraju
/source/direction/monitor/product-analytics/ @david @justinfarris @stkerr
/source/direction/monitor/observability/ @sguyon

^[Product - SaaS Platforms Section]
/source/direction/saas-platforms/ @fzimmer @david @gl-product-leadership
/source/direction/saas-platforms/delivery/ @fzimmer @swiskow
/source/direction/saas-platforms/scalability/ @fzimmer @swiskow
/source/direction/saas-platforms/dedicated/ @fzimmer @awthomas
/source/direction/saas-platforms/switchboard/ @fzimmer @lbortins

^[Product - Core Platform Section]
/source/direction/core_platform/ @joshlambert @david @gl-product-leadership
/source/direction/core_platform/tenant-scale/ @joshlambert @lohrc
/source/direction/core_platform/tenant-scale/cell/ @joshlambert @lohrc
/source/direction/core_platform/tenant-scale/organization/ @joshlambert @lohrc
/source/direction/core_platform/tenant-scale/groups-&-projects/ @joshlambert @lohrc
/source/direction/core_platform/tenant-scale/user-profile/ @joshlambert @lohrc
/source/direction/geo/ @joshlambert @sranasinghe
/source/direction/geo/geo_replication/ @joshlambert @sranasinghe
/source/direction/geo/backup_restore/ @joshlambert  @sranasinghe
/source/direction/geo/disaster_recovery/ @joshlambert @sranasinghe
/source/direction/global-search/ @joshlambert
/source/direction/cloud-connector/ @joshlambert @rogerwoo
/source/direction/distribution/ @joshlambert @dorrino
/source/direction/core_platform/dotcom/ @joshlambert
/source/direction/database/ @joshlambert
/source/direction/gitaly/ @joshlambert @mjwood

^[Product - Fulfillment Section]
/source/direction/fulfillment/ @ofernandez2
/source/direction/fulfillment/utilization/ @alex_martin
/source/direction/fulfillment/purchase/ @ppalanikumar
/source/direction/fulfillment/provision/ @courtmeddaugh
/source/direction/fulfillment/platform/ @ofernandez2
/source/direction/fulfillment/subscription-management/ @tgolubeva
/sites/uncategorized/source/pricing/licensing-faq/cloud-licensing/ @courtmeddaugh

^[Product Section - Data Science]
/source/direction/data-science/ @tmccaslin @hbenson @gl-product-leadership
/source/direction/modelops/ @tmccaslin @hbenson @gl-product-leadership
/source/direction/ai-powered/ @tmccaslin @hbenson @gl-product-leadership
/source/direction/ai-powered/ai_framework/ @tmccaslin @hbenson @tlinz @gl-product-leadership
/source/direction/ai-powered/duo_chat/ @tmccaslin @hbenson @tlinz @gl-product-leadership
/source/direction/ai-powered/ai_model_validation/ @tmccaslin @hbenson @gl-product-leadership

^[Product Section - Service Management]
/source/direction/service_management/ @hbenson

^[Product Section - Other]
/source/direction/product-analysis/ @cbraza @gl-product-leadership
/source/direction/mobile/mobile-devops/ @darbyfrey @bmarnane @dcroft

^[Recruiting]
/sites/uncategorized/source/jobs/ @rallen3 @drogozinski

^[Security Compliance]
/sites/uncategorized/source/security/ @joshlemos @jlongo_gitlab

^[Releases blog posts]
/sites/uncategorized/source/releases/posts/ @gitlab-org/delivery, @gl-product-leadership

^[Delivery team]
/data/releases.yml @gitlab-org/delivery

^[Speakers Bureau]
/sites/uncategorized/source/speakers/ @abuango @johncoghlan @dnsmichi
/sites/uncategorized/source/speakers/index.html.erb @abuango @johncoghlan @dnsmichi
/sites/uncategorized/source/speakers/data.json.erb @abuango @johncoghlan @dnsmichi
/data/speakers.yml @abuango @johncoghlan @dnsmichi
/source/frontend/components/speakers-bureau/ @abuango @johncoghlan @dnsmichi
/source/frontend/components/speakers-bureau/SpeakersBureau.vue @abuango @johncoghlan @dnsmichi

^[Environment Setup Script]
/scripts/setup-macos-dev-environment.sh @cmestel

^[Ruby Version]
.ruby-version @mpreuss22 @laurenbarker @cwoolley-gitlab
