---
layout: markdown_page
title: Efficient GitLab SaaS Free tier FAQ
description: "On this page you can view frequently asked questions for changes related to Free tier efficiency"
canonical_path: "/pricing/faq-efficient-free-tier/"
---

# Frequently Asked Questions - GitLab SaaS Free Tier

{:.no_toc}

### On this page

{:.no_toc}

{:toc}

- TOC

## Effective Date of the changes

#### Q: What is the effective date of the changes?
{:.no_toc}

We intend to roll out the application of user and storage limits independently and gradually.

User limits went into effect on 2023-06-13 for existing top-level private groups created before 2022-12-28. Storage limits have yet to be applied. 

### Next steps

#### Over both the Free storage limit and the [5-user limit](#user-limits-on-gitlab-saas-free-tier):

If your top-level group is above **both the 5-user limit and Free storage limit**, it's important you manage your user count first, as the 5-user limit has already been fully rolled out. **If you reduce your storage usage and/or buy more storage while your user count is more than 5, and the namespace is still on the Free tier, your top-level group would be immediately placed in the [read only state](https://docs.gitlab.com/ee/user/read_only_namespaces.html).** Please see the recommended ordering of considerations below:

1. Apply for [GitLab for Education](/solutions/education/join/), [GitLab for Open Source](/solutions/open-source/join/), or [GitLab for Startups](/solutions/startups/) if you meet the eligibility requirements.
1. Consider using a [self-managed instance](https://docs.gitlab.com/ee/subscriptions/self_managed/) of GitLab which does not have these limits on the Free tier.
1. [Start a trial](/free-trial/) or [upgrade to GitLab Premium or Ultimate](/pricing/) which include higher limits and features that enable growing teams to ship faster without sacrificing on quality.
1. Reduce storage consumption by following the suggestions in the [Managing Your Storage](#managing-your-storage-usage) section of this page.
1. [Purchase additional storage](https://docs.gitlab.com/ee/subscriptions/gitlab_com/#purchase-more-storage-and-transfer) units at $60/year for 10GB of storage.
1. [Talk to an expert](https://page.gitlab.com/usage_limits_help.html) to learn more about your options and ask questions.

### Over only the Free storage limit

If your top-level group is only over the Free storage limit, it's recommended you manage your storage usage first. Please see the recommended ordering of considerations below:

1. Reduce storage consumption by following the suggestions in the [Managing Your Storage](#managing-your-storage-usage) section of this page.
1. Apply for [GitLab for Education](/solutions/education/join/), [GitLab for Open Source](/solutions/open-source/join/), or [GitLab for Startups](/solutions/startups/) if you meet the eligibility requirements.
1. Consider using a [self-managed instance](https://docs.gitlab.com/ee/subscriptions/self_managed/) of GitLab which does not have these limits on the Free tier.
1. [Purchase additional storage](https://docs.gitlab.com/ee/subscriptions/gitlab_com/#purchase-more-storage-and-transfer) units at $60/year for 10GB of storage.
1. [Start a trial](/free-trial/) or [upgrade to GitLab Premium or Ultimate](/pricing/) which include higher limits and features that enable growing teams to ship faster without sacrificing on quality.
1. [Talk to an expert](https://page.gitlab.com/usage_limits_help.html) to learn more about your options and ask questions.

### Next Steps

- Apply for [GitLab for Education](/solutions/education/join/), [GitLab for Open Source](/solutions/open-source/join/), or [GitLab for Startups](/solutions/startups/) if you meet the eligibility requirements.
- Consider using a [self-managed instance](https://docs.gitlab.com/ee/subscriptions/self_managed/) of GitLab which does not have these limits on the Free tier.
- [Start a trial](/free-trial/) or [upgrade to GitLab Premium or Ultimate](/pricing/) which include higher limits and features that enable growing teams to ship faster without sacrificing on quality.
- [Talk to an expert](https://page.gitlab.com/usage_limits_help.html) to learn more about your options and ask questions.

## Public projects on GitLab SaaS Free Tier

### Overview

#### Q: What is changing with public projects?
{:.no_toc}

The GitLab for Open Source Program was created to give back to the open source community by encouraging individuals and teams to contribute to open source. Public open source projects will need to be part of the GitLab for Open Source program to continue to receive GitLab Ultimate features.

As previously announced, [all Free tier public projects will not receive Ultimate by default](/blog/2022/02/04/ultimate-perks-for-open-source-projects/), and [all public projects will be subject to quota of compute minutes applicable to their plan](/blog/2021/11/11/public-project-minute-limits/). Free tier users using GitLab for public open source projects should consider applying to the [GitLab for Open Source program](/solutions/open-source/) to continue to take advantage of GitLab Ultimate features such as portfolio management, advanced security testing, security risk mitigation, and compliance.

#### Q: Which users are these changes applicable to?
{:.no_toc}

These changes are applicable to users with public projects on the Free tier of GitLab SaaS. These changes do not apply to self-managed free and paid tier users, SaaS paid tier users, and community programs - including GitLab for Open Source, Education and Startups users.

#### Q: What is the effective date of the changes?
{:.no_toc}

The CI/CD limits on public projects will be applicable for all users including paid and Free tier with public projects starting 2022-06-01. Public projects on the Free tier will stop receiving Ultimate entitlements by default starting 2022-07-01.

#### Q: How can I retain the Ultimate entitlements for my public projects?
{:.no_toc}

There are two ways:
- Enroll in the GitLab Open Source Program. [Learn more](/solutions/open-source/join/) on how to apply to the GitLab for Open Source Program.
- Upgrade to GitLab Ultimate. Learn more [here](/pricing/).

### Managing Compute Usage

#### Q. What happens when my top-level group or personal namespace has used all of the month’s allocated compute minutes limit of 400 minutes?
{:.no_toc}

You will not be able to run new jobs until you purchase additional compute minutes, or until the next month when you receive your monthly allotted compute minutes. Running jobs will be cancelled when a top-level group or personal namespace reaches its limit while a pipeline is running.

#### Q: How can I manage my compute minutes usage to stay under the limit?
{:.no_toc}

This detailed FAQ covers how to manage your compute minutes usage.
Watch this deep dive video on how you can manage your compute minutes usage.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/GrO-8KtIpRA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

#### Q. What exactly is a compute minute and how is it calculated?
{:.no_toc}

Compute minutes are calculated using a formula that includes the job duration and an applied cost factor. Please refer to the [documentation](https://docs.gitlab.com/ee/ci/pipelines/cicd_minutes.html#how-cicd-minute-usage-is-calculated) to learn more.

#### Q: I am an active contributor to GitLab. Will the same limits be applicable to me as well?
{:.no_toc}

All Free tier users receive 50,000 compute minutes for running pipelines on public forks of public open source projects, like GitLab. Contributions to all other projects by Free tier users are subject to the new limits.

### GitLab for Open Source Program

#### Q: How can I apply for the GitLab for Open Source program?
{:.no_toc}

The GitLab for Open Source Program gives access to unlimited seats per license of GitLab Ultimate (SaaS or Self-Managed), including 50,000 compute minutes, excluding support.  View the program requirements and apply for the GitLab for Open Source program online here. In most cases, GitLab will respond with a decision or request for additional information within 15  business days.

#### Q: What happens to my account if I do not enroll in the GitLab for Open Source program or I am not accepted into the program?
{:.no_toc}

If you are not part of the GitLab for Open Source program, after July 1, 2022, your account will stop receiving GitLab Ultimate capabilities and will receive the entitlements available to the Free tier.
You will not lose any data, but you will not be able to create any new jobs or artifacts nor access Ultimate features if you have exceeded the usage limits of the Free tier.

#### Q: Can I apply for GitLab’s Open Source program with some public projects and some private projects in my group?
{:.no_toc}

No. In order to meet the [requirements of the Open Source Program](/solutions/open-source/join/), **all** of the code you host in this GitLab group must be published under [OSI-approved open source licences](https://opensource.org/licenses/category). All projects under this group must have public visibility. Individual projects are not eligible for the Open Source Program. Additionally, your organization must not seek to make a profit. Please check all the eligibility requirements for the Open Source program [here](/solutions/open-source/join/).

#### Q: Do I need to apply for GitLab for Open Source Program for each individual project?
{:.no_toc}

No. You will need to apply for GitLab for Open Source Program at the top-level [group](https://docs.gitlab.com/ee/user/group/#namespaces).

- All projects under this group must have public visiblity
- All projects must be published under [OSI-approved open source licences](https://opensource.org/licenses/category).
- All projects must be within a group or a subgroup, you cannot apply for the program for projects within a personal namespace. (for example, consider a user `username`. Projects within the username group `username` will not be considered. If `username` creates a group `user-group` and has open source projects within that group, then `user-group` can be considered for the Open Source program)
- If you have projects unrelated to an `open source project` (for example, personal projects), you will need to move out the unrelated projects to a different group to be eligible for the GitLab for Open Source Program.

### Next Steps

- [Click here](/solutions/open-source/join/) to join the GitLab for Open Source program
- [Purchase online](https://customers.gitlab.com) or contact [GitLab Sales](/sales/) to upgrade to a paid GitLab.com tier - which have advanced capabilities and higher usage limits.

The active user.
