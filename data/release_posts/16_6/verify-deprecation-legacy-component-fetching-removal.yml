---
features:
  primary:
  - name: "CI/CD components Beta release"
    available_in: [core, premium, ultimate]
    documentation_link: 'https://docs.gitlab.com/ee/ci/components/'
    image_url: '/images/16_6/components.png'
    reporter: dhershkovitch
    stage: verify
    categories:
    - Pipeline Composition
    issue_url: 'https://gitlab.com/groups/gitlab-org/-/epics/9897'
    description: |
      In GitLab 16.1, we [announced](https://about.gitlab.com/blog/2023/07/10/introducing-ci-components/) the release of an exciting experimental feature called CI/CD components. The component is a pipeline building block that can be listed in the upcoming CI/CD catalog.

      Today we are excited to announce the Beta availability of CI/CD components. With this release, we have also improved the components folder structure from the initial experimental version. If you are already testing the experimental version of CI/CD components, it's essential to migrate to the [new folder structure](https://docs.gitlab.com/ee/ci/components/#directory-structure). You can see some examples [here](https://gitlab.com/gitlab-components/). The old folder structure is deprecated and we plan to remove it within the next couple of releases.

      If you try out CI/CD components, you are also welcome to try the new CI/CD catalog, currently available as an experimental feature. You can search the [Global CI/CD catalog](https://docs.gitlab.com/ee/ci/components/catalog.html) for components that others have created and published for public use. Additionally, if you create your own components, you can choose to publish them in the catalog too!
