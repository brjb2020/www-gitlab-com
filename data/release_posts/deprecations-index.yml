---
'16.7':
- JWT `/-/jwks` instance endpoint is deprecated
- 'Dependency Proxy: Access tokens to have additional scope checks'
- List repository directories Rake task
'16.6':
- The GitHub importer Rake task
- Proxy-based DAST deprecated
- 'GraphQL: deprecate support for `canDestroy` and `canDelete`'
- Breaking change to the Maven repository group permissions
- File type variable expansion fixed in downstream pipelines
- Legacy Geo Prometheus metrics
- Container registry support for the Swift and OSS storage drivers
'16.5':
- Offset pagination for `/users` REST API endpoint is deprecated
- Security policy field `newly_detected` is deprecated
'16.4':
- Internal container registry API tag deletion endpoint
- "`postgres_exporter['per_table_stats']` configuration setting"
- The `ci_job_token_scope_enabled` projects API attribute is deprecated
- Deprecate change vulnerability status from the Developer role
- 'Geo: Legacy replication details routes for designs and projects deprecated'
'16.3':
- RSA key size limits
- Deprecate field `hasSolutions` from GraphQL VulnerabilityType
- Twitter OmniAuth login option is deprecated from self-managed GitLab
- 'Geo: Housekeeping Rake tasks'
- Twitter OmniAuth login option is removed from GitLab.com
- Job token allowlist covers public and internal projects
- GraphQL field `totalWeight` is deprecated
'16.2':
- GraphQL field `registrySizeEstimated` has been deprecated
- Deprecate `CiRunner` GraphQL fields duplicated in `CiRunnerManager`
- OmniAuth Facebook is deprecated
- The pull-based deployment features of the GitLab agent for Kubernetes is deprecated
- Deprecated parameters related to custom text in the sign-in page
'16.1':
- Deprecate Windows CMD in GitLab Runner
- Unified approval rules are deprecated
- Running a single database is deprecated
- GraphQL deprecation of `dependencyProxyTotalSizeInBytes` field
- Deprecate `message` field from Vulnerability Management features
'16.0':
- GitLab administrators must have permission to modify protected branches or tags
- Changing MobSF-based SAST analyzer behavior in multi-module Android projects
- GraphQL type, `RunnerMembershipFilter` renamed to `CiRunnerMembershipFilter`
- CiRunnerUpgradeStatusType GraphQL type renamed to CiRunnerUpgradeStatus
- "`sidekiq` delivery method for `incoming_email` and `service_desk_email` is deprecated"
- PostgreSQL 13 deprecated
- Bundled Grafana deprecated and disabled
- CiRunner.projects default sort is changing to `id_desc`
'15.9':
- Default CI/CD job token (`CI_JOB_TOKEN`) scope changed
- Trigger jobs can mirror downstream pipeline status exactly
- CI/CD jobs will fail when no secret is returned from Hashicorp Vault
- HashiCorp Vault integration will no longer use CI_JOB_JWT by default
- Legacy URLs replaced or removed
- Support for Praefect custom metrics endpoint configuration
- Secure scanning CI/CD templates will use new job `rules`
- Required Pipeline Configuration is deprecated
- Development dependencies reported for PHP and Python
- Secure analyzers major version update
- SAST analyzer coverage changing in GitLab 16.0
- "`omniauth-authentiq` gem no longer available"
- License Compliance CI Template
- Managed Licenses API
- License-Check and the Policies tab on the License Compliance page
- Legacy Praefect configuration method
- Default CI/CD job token (`CI_JOB_TOKEN`) scope changed
- Embedding Grafana panels in Markdown is deprecated
- GitLab Runner platforms and setup instructions in GraphQL API
- Queue selector for running Sidekiq is deprecated
- Filepath field in Releases and Release Links APIs
- Option to delete projects immediately is deprecated from deletion protection settings
- The GitLab legacy requirement IID is deprecated in favor of work item IID
- External field in GraphQL ReleaseAssetLink type
- External field in Releases and Release Links APIs
- Single database connection is deprecated
- Enforced validation of CI/CD parameter character lengths
- Deprecation and planned removal for `CI_PRE_CLONE_SCRIPT` variable on GitLab SaaS
- Old versions of JSON web tokens are deprecated
- Slack notifications integration
'15.8':
- Developer role providing the ability to import projects to a group
- The Visual Reviews tool is deprecated
- The API no longer returns revoked tokens for the agent for Kubernetes
- Non-standard default Redis ports are deprecated
- Configuring Redis config file paths using environment variables is deprecated
- Use of third party container registries is deprecated
- The latest Terraform templates will overwrite current stable templates
- "`environment_tier` parameter for DORA API"
- Maintainer role providing the ability to change Package settings using GraphQL API
- 'GraphQL: The `DISABLED_WITH_OVERRIDE` value of the `SharedRunnersSetting` enum
  is deprecated. Use `DISABLED_AND_OVERRIDABLE` instead'
- Container registry pull-through cache
- Deployment API returns error when `updated_at` and `updated_at` are not used together
- Cookie authorization in the GitLab for Jira Cloud app
- Projects API field `operations_access_level` is deprecated
- Live Preview no longer available in the Web IDE
- GitLab Helm chart values `gitlab.kas.privateApi.*` are deprecated
- Auto DevOps support for Herokuish is deprecated
- Limit personal access token and deploy token's access with external authorization
- Automatic backup upload using Openstack Swift and Rackspace APIs
- Conan project-level search endpoint returns project-specific results
- Azure Storage Driver defaults to the correct root prefix
- Auto DevOps no longer provisions a PostgreSQL database by default
- Dependency Scanning support for Java 13, 14, 15, and 16
'15.7':
- "`POST ci/lint` API endpoint deprecated"
- DAST report variables deprecation
- Support for periods (`.`) in Terraform state names might break existing states
- Shimo integration
- The Phabricator task importer is deprecated
- ZenTao integration
- DAST API variables
- KAS Metrics Port in GitLab Helm Chart
- DAST ZAP advanced configuration variables deprecation
- The `gitlab-runner exec` command is deprecated
- DAST API scans using DAST template is deprecated
- Support for REST API endpoints that reset runner registration tokens
'15.6':
- GitLab Runner registration token in Runner Operator
- Registration tokens and server-side runner arguments in `gitlab-runner register`
  command
- Configuration fields in GitLab Runner Helm Chart
- "`runnerRegistrationToken` parameter for GitLab Runner Helm Chart"
- Registration tokens and server-side runner arguments in `POST /api/v4/runners` endpoint
'15.5':
- File Type variable expansion in `.gitlab-ci.yml`
- vulnerabilityFindingDismiss GraphQL mutation
- GraphQL field `confidential` changed to `internal` on notes
'15.4':
- Starboard directive in the config for the GitLab Agent for Kubernetes
- Non-expiring access tokens
- Toggle behavior of `/draft` quick action in merge requests
- Container Scanning variables that reference Docker
- Vulnerability confidence field
'15.3':
- Security report schemas version 14.x.x
- Atlassian Crowd OmniAuth provider
- Use of `id` field in vulnerabilityFindingDismiss mutation
- Redis 5 deprecated
- CAS OmniAuth provider
'15.2':
- Remove `job_age` parameter from `POST /jobs/request` Runner endpoint
'15.10':
- Environment search query requires at least three characters
- DingTalk OmniAuth provider
- Bundled Grafana Helm Chart is deprecated
- Work items path with global ID at the end of the path is deprecated
- Major bundled Helm Chart updates for the GitLab Helm Chart
- Legacy Gitaly configuration method
- Deprecated Consul http metrics
'15.1':
- Jira DVCS connector for Jira Cloud
- PipelineSecurityReportFinding projectFingerprint GraphQL field
- PipelineSecurityReportFinding name GraphQL field
- project.pipeline.securityReportFindings GraphQL query
'15.0':
- GraphQL API legacyMode argument for Runner status
- PostgreSQL 12 deprecated
'14.9':
- GitLab self-monitoring project
- Integrated error tracking disabled by default
- "`user_email_lookup_limit` API field"
- htpasswd Authentication for the container registry
- Permissions change for downloading Composer dependencies
- GraphQL permissions change for Package settings
- Background upload for object storage
'14.8':
- OAuth tokens without expiration
- Querying Usage Trends via the `instanceStatisticsMeasurements` GraphQL node
- Test coverage project CI/CD setting
- Dependency Scanning Python 3.9 and 3.6 image deprecation
- Container Network and Host Security
- Secure and Protect analyzer major version update
- Out-of-the-box SAST support for Java 8
- Secure and Protect analyzer images published in new location
- Request profiling
- SAST analyzer consolidation and CI/CD template changes
- SAST support for .NET 2.1
- Deprecate feature flag PUSH_RULES_SUPERSEDE_CODE_OWNERS
- Vulnerability Check
- Support for gRPC-aware proxy deployed between Gitaly and rest of GitLab
- "`started` iteration state"
- GraphQL ID and GlobalID compatibility
- GraphQL networkPolicies resource deprecated
- Deprecate legacy Gitaly configuration methods
- Optional enforcement of SSH expiration
- Retire-JS Dependency Scanning tool
- Optional enforcement of PAT expiration
- External status check API breaking changes
- "`projectFingerprint` in `PipelineSecurityReportFinding` GraphQL"
- Required pipeline configurations in Premium tier
- Elasticsearch 6.8
- "`CI_BUILD_*` predefined variables"
'14.7':
- Tracing in GitLab
- Monitor performance metrics through Prometheus
- Logging in GitLab
- Sidekiq metrics and health checks configuration
- "`artifacts:reports:cobertura` keyword"
'14.6':
- "`type` and `types` keyword in CI/CD configuration"
- apiFuzzingCiConfigurationCreate GraphQL mutation
- CI/CD job name length limit
- Legacy approval status names from License Compliance API
- bundler-audit Dependency Scanning tool
'14.5':
- "`pipelines` field from the `version` field"
- GraphQL API Runner status will not return `paused`
- "`dependency_proxy_for_private_groups` feature flag"
- Package pipelines in API payload is paginated
- Support for SLES 12 SP2
- Update to the container registry group-level API
- "`promote-to-primary-node` command from `gitlab-ctl`"
- "`promote-db` command from `gitlab-ctl`"
- "`defaultMergeCommitMessageWithDescription` GraphQL API field"
- Known host required for GitLab Runner SSH executor
- "`Versions` on base `PackageType`"
- Changing an instance (shared) runner to a project (specific) runner
- Value Stream Analytics filtering calculation change
- SaaS certificate-based integration with Kubernetes
- Self-managed certificate-based integration with Kubernetes
'14.3':
- GitLab Serverless
- OmniAuth Kerberos gem
- Legacy database configuration
- Audit events for repository push events
'14.10':
- Dependency Scanning default Java version changed to 17
- Outdated indices of Advanced Search migrations
- Toggle notes confidentiality on APIs
'14.0':
- OAuth implicit grant
- Changing merge request approvals with the `/approvals` API endpoint
